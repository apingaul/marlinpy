# Marlinpy

A python interface to ILCSoft marlin package. aka, configure Marlin through json or yaml instead of xml.

Yes it is overly complicated for no apparent reason, yes I would do it differently if I had to do it again ;), but it works as is and is easy to use/integrate in other python projects.

## Installation

``` bash
git clone https://gitlab.cern.ch/apingaul/marlinpy
cd marlinpy
# Install the package + dependencies with pip
pip install .
```

You might see the following error message during compilation:

``` bash

In file included from ext/_yaml.c:596:0:
ext/_yaml.h:2:18: fatal error: yaml.h: No such file or directory
 #include <yaml.h>
                  ^
compilation terminated.
Error compiling module, falling back to pure Python

```

As written on the last line this is not a problem and can be safely ignored.  
It just means the c++ dependency to this package (`libyaml-dev` on debian) are not present on the system and it will fall back to using a pure python implementation.

## Usage

Marlin options are passed with roughly the same structure as the xml steering files but in either yaml or json format:

### With python/json configuration

Processor parameters:

```python
marlin_verb="MESSAGE"
dict_with_marlin_options={
    "global":{
            "Verbosity": marlin_verb,
            "MaxRecordNumber": 0,
            "SkipNEvents": 0
    },
    "MyFancySDHCALProcessor":{
        "InputCollectionNames": "DHCALRawHits",
        "Verbosity": marlin_verb,
        "Param1": 42
    }
}
```

Running the processor :

```python
from marlinpy import Marlin

marlin = Marlin(xmlConfig='/path/to/marlin/steering.xml',
                        libraries=['/path/to/lib1.so', '/path/to/lib2.so'],
                        ilcSoftInitScript='/path/to/init_ilcsoft.sh,
                        outputPath='/path/to/outputdir',
                        outputFiles=['output_file1.slcio', 'output_file2.slcio', etc.],
                        cliOptions=dict_with_marlin_options)

config_file='/path/to/new/config.yml'
marlin.writeConfigFile(config_file)
marlin.run(config_file)
```

### With python/yaml configuration

```yaml
Marlin:
  cliOptions:
    MyFancySDHCALProcessor.InputCollectionNames: DHCALRawHits
    MyFancySDHCALProcessor.Verbosity: MESSAGE
    MyFancySDHCALProcessor.Param1: 42
    global.Verbosity: MESSAGE
    global.MaxRecordNumber: 0
    global.SkipNEvents: 0
  ilcSoftInitScript: /path/to/init_ilcsoft.sh
  libraries: ['/path/to/lib1.so', '/path/to/lib2.so']
  xmlConfig: '/path/to/marlin/steering.xml'
```

```python
from marlinpy import Marlin

config_file='/path/to/config.yml'
marlin = Marlin()
marlin.run(config_file)
```
