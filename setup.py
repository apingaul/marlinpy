#!/usr/bin/env python

from setuptools import setup, find_packages


def get_version():
    g = {}
    exec (open("marlinpy/version.py").read(), g)
    return g["__version__"]


install_deps = ['future; python_version<"3"', 'PyYAML', 'argparse', 'loguru; python_version>"3.5"']

test_deps = ['mock', 'pytest', 'tox']

setup(
    name='marlinpy',
    author='Antoine Pingault',
    author_email='antoine.pingault@cern.ch',
    maintainer='Antoine Pingault',
    maintainer_email='antoine.pingault@cern.ch',
    version=get_version(),
    description='Python interface for ilcsoft Marlin',
    url='https://gitlab.cern.ch/apingaul/marlinpy',
    license='MIT',
    packages=find_packages(),
    install_requires=install_deps,
    extras_require={"testing": test_deps},
    python_requires=">=2.7",
    entry_points={'console_scripts': ['marlinpy = marlinpy.__main__:main']},
    platforms="Any",
)
