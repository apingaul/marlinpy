#!/usr/bin/env python
#

from __future__ import (absolute_import, division, print_function, unicode_literals)

import os
import sys
import logging
import subprocess

import yaml


class Marlin(object):
    def __init__(self,
                 xmlConfig=None,
                 runOnGrid=False,
                 libraries=[],
                 ilcSoftInitScript=None,
                 uploadScript='',
                 downloadScript='',
                 outputPath=None,
                 outputFiles=[],
                 cliOptions={}):
        self.xmlConfig = xmlConfig
        self.runOnGrid = runOnGrid
        self.libraries = libraries
        self.ilcSoftInitScript = ilcSoftInitScript
        self.uploadScript = uploadScript
        self.downloadScript = downloadScript
        self.outputPath = outputPath
        self.outputFiles = outputFiles
        self.cliOptions = cliOptions
        self.log = logging.getLogger('marlinpy')

    def setXMLConfig(self, xmlFile):
        ''' Set xml config file
        '''
        self.xmlConfig = xmlFile

    def setRunOnGrid(self, runOnGrid):
        ''' Set xml config file
        '''
        self.runOnGrid = runOnGrid

    def setLibraries(self, libraries):
        ''' Set dll libraries
        '''
        self.libraries = libraries

    def setCliOptions(self, options):
        ''' Set all options to be superseded in xml
        '''
        for k, v in options.items():
            if isinstance(v, dict):
                self.setCliOptions(v)
            self.setCliOption(k, v)

    def getFormattedCliOptions(self):
        ''' Returns cliOptions as a string
        '''
        return self.formatCliOptions(self.cliOptions)

    def formatCliOptions(self, options, topLevelKey=None):
        cliOpt = ''
        for key, value in options.items():
            cliKey = key if topLevelKey is None else topLevelKey + '.' + key
            if isinstance(value, dict):
                cliOpt += self.formatCliOptions(value, topLevelKey=cliKey)
            else:
                cliOpt += '--{0}="{1}" '.format(cliKey, value if not isinstance(value, list) else str(' '.join(value)))
        return cliOpt

    def setCliOption(self, option, value):
        ''' Set One option to be superseded in xml
        '''
        self.cliOptions[option] = value

    def setILCSoftScript(self, script):
        ''' Set init_ilcsoft.sh script to source for proper software linking
        '''
        self.ilcSoftInitScript = script

    def setUploadScript(self, script):
        ''' Set upload script to grid
        '''
        self.uploadScript = script

    def setDownloadScript(self, script):
        ''' Set Download script to grid
        '''
        self.downloadScript = script

    def setOutputPath(self, path):
        ''' Set Download script to grid
        '''
        self.outputPath = path

    def setOutputFiles(self, files):
        ''' Set Download script to grid
        '''
        self.outputFiles = files

    def writeConfigFile(self, cfgFile):
        ''' Write marlin specific config into configFile
            update file if found, create it otherwise
        '''
        try:
            with open(cfgFile, 'r') as ymlFile:
                cfg = yaml.safe_load(ymlFile)
        except IOError:
            # print(Creating new configuration file '{0}'.format(cfgFile))
            cfg = {}
            if not os.path.exists(os.path.dirname(cfgFile)):
                os.makedirs(os.path.dirname(cfgFile))
        else:  # found existing config file
            if cfg is None:  # empty config file
                cfg = {}
        with open(cfgFile, 'w') as ymlFile:
            marlinSection = {}
            cfg['Marlin'] = marlinSection
            marlinSection['xmlConfig'] = self.xmlConfig
            marlinSection['libraries'] = self.libraries
            marlinSection['ilcSoftInitScript'] = self.ilcSoftInitScript
            marlinSection['uploadScript'] = self.uploadScript
            marlinSection['downloadScript'] = self.downloadScript
            marlinSection['outputPath'] = self.outputPath
            marlinSection['outputFiles'] = self.outputFiles

            if self.cliOptions is not None:
                cliOptionSubSection = {}
                marlinSection['cliOptions'] = cliOptionSubSection

                for key, value in self.cliOptions.items():
                    cliOptionSubSection[key] = value

            ymlFile.write(yaml.safe_dump(cfg, default_flow_style=False))

    def readConfigFile(self, cfgFile):
        ''' Read Marlin specific config in cfgFile
            Exit if cfgFile not found
        '''
        try:
            with open(cfgFile, "r") as ymlFile:
                cfg = yaml.safe_load(ymlFile)
        except IOError:
            sys.exit(self.log.error("Config file '{}' not found".format(cfgFile)))

        try:
            marlinSection = cfg['Marlin']
            self.xmlConfig = marlinSection['xmlConfig']
            self.libraries = marlinSection['libraries']
            self.ilcSoftInitScript = marlinSection['ilcSoftInitScript']
            self.uploadScript = marlinSection['uploadScript']
            self.downloadScript = marlinSection['downloadScript']
            self.outputPath = marlinSection['outputPath']
            self.outputFiles = marlinSection['outputFiles']
            self.cliOptions = marlinSection['cliOptions']
        except KeyError as exc:
            self.log.error("Key %s not found in cfgFile" % exc)

    def checkConfig(self, cfgFile):
        ''' Check that core config are set
            Exit otherwise
        '''
        _, file_extension = os.path.splitext(cfgFile)
        if file_extension != '.yml':
            raise IOError(self.log.error("config file should be in a yml format, found '%s'" % file_extension))

        self.readConfigFile(cfgFile)
        missingConfig = []
        for k, v in vars(self).items():
            if v is None:
                missingConfig.append(k)
            if missingConfig:
                sys.exit(self.log.error("Some configuration are missing from '%s' : %s" % (cfgFile, missingConfig)))

    def run(self, cfgFile):
        self.checkConfig(cfgFile)
        # source ilcsoft for proper environment
        cmd = "source {0}; ".format(self.ilcSoftInitScript)
        cmd += "Marlin "

        # add eventual replacement options to configFile
        self.log.debug('cliOptions: %s' % self.cliOptions)

        if self.cliOptions:
            cmd += self.getFormattedCliOptions()

        # Add configuration file
        cmd += self.xmlConfig
        self.log.info("Running Marlin with cmd '%s'" % cmd)
        self.log.info("MARLIN_DLL=%s" % ':'.join(self.libraries))
        try:
            # Run marlin and send its output line by line to the logger
            proc = subprocess.Popen(cmd,
                                    env=dict(os.environ, MARLIN_DLL=':'.join(self.libraries)),
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT,
                                    shell=True,
                                    universal_newlines=True,
                                    bufsize=1)
            while True:
                output = proc.stdout.readline()
                if output == '' and proc.poll() is not None:
                    break
                if output:
                    self.log.info(output.strip())
            return proc.poll()
        except subprocess.CalledProcessError as e:
            self.log.error('Marlin crashed with following error', exc_info=True)

    def uploadFiles(self):
        if self.runOnGrid:
            for f in self.outputFiles:
                cmd = "source {} {} ./ {}".format(self.uploadScript, f, self.outputPath)
                self.log.info("uploading '%s' to '%s' with cmd '%s'" % (f, self.outputPath, cmd))
                subprocess.check_call(cmd, env=dict(os.environ), shell=True)
