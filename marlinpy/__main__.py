#!/usr/bin/env python
'''
    Executable for marlinpy, install then run as `marlinpy path/to/configFile.yml`
'''
from __future__ import (absolute_import, division, print_function, unicode_literals)

import sys
import os
import logging
import argparse

from marlinpy import Marlin


def main(args=None):

    log = logging.getLogger('marlinpy')
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    log.setLevel(logging.DEBUG)

    parser = argparse.ArgumentParser(description='Python interface to marlin', prog='marlinpy')
    parser.add_argument('configFile', help='yml config file')
    args = parser.parse_args()

    log.info("Running with configuration file '%s'" % args.configFile)
    marlin = Marlin()
    marlin.run(args.configFile)
    marlin.uploadFiles()


if __name__ == "__main__":
    main()
